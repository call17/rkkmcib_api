<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" encoding="UTF-8"/>
	
	<xsl:template match="node()|@*">
		<xsl:copy>
		    <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
	
	<xsl:template match="testcase | testsuite">
		<xsl:copy>
			<xsl:attribute name="classname"><xsl:value-of select="@name"/></xsl:attribute>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="time"><xsl:value-of select="translate(@time, '.', ',')"/></xsl:attribute>

			<xsl:apply-templates select="node()"/>
		</xsl:copy>
    </xsl:template>
	
</xsl:stylesheet>